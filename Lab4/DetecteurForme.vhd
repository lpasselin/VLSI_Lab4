----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:27:38 12/04/2017 
-- Design Name: 
-- Module Name:    DetecteurForme - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DetecteurForme is
    Port ( CLK : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           PERIM : in  STD_LOGIC_VECTOR (15 downto 0);
           AIRE : in  STD_LOGIC_VECTOR (17 downto 0); 
           FORME : out  STD_LOGIC_VECTOR (1 downto 0);
			  RESULTAT_RDY : out STD_LOGIC);
end DetecteurForme;

architecture Behavioral of DetecteurForme is

component Divider is
  port (
    rfd : out STD_LOGIC; 
    rdy : out STD_LOGIC; 
    nd : in STD_LOGIC; 
    clk : in STD_LOGIC; 
    dividend : in STD_LOGIC_VECTOR ( 31 downto 0 ); 
    quotient : out STD_LOGIC_VECTOR ( 31 downto 0 ); 
    divisor : in STD_LOGIC_VECTOR ( 17 downto 0 ); 
    fractional : out STD_LOGIC_VECTOR ( 15 downto 0 ) 
  );
end component;

component Multiplier is
  PORT (
    clk : IN STD_LOGIC;
    a : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    b : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    p : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END component;

type etat is (attente, multiplication, division, detect_forme, fin);
signal etat_act : etat := attente;
signal perim_temp : std_logic_vector(15 downto 0) := x"0000";
signal aire_temp : std_logic_vector(17 downto 0) := "00" & x"0001"; --Pour �viter div par 0
signal resultat, resultat_temp : std_logic_vector(31 downto 0) := x"00000000";
signal perim_carre, perim_carre_temp : std_logic_vector(31 downto 0) := x"00000000"; 
signal res_div_rdy: std_logic := '0';
signal enable_div: std_logic := '0';

begin

	mul: Multiplier port map (clk => CLK, a => perim_temp, b => perim_temp, p => perim_carre);
	div: Divider port map (rdy => res_div_rdy, nd => enable_div, clk => CLK, dividend => perim_carre_temp, quotient => resultat, divisor => aire_temp);
	
	state_machine: process(CLK,RESET,PERIM,AIRE)
	begin
		if (RESET = '0') then
			RESULTAT_RDY <= '0';
			FORME <= "00";
			perim_temp <= x"0000";
			aire_temp <= "00" & x"0001";
		elsif (CLK'event and CLK = '1') then
			case etat_act is
				when attente =>
					RESULTAT_RDY <= '0';
					if (PERIM /= perim_temp or AIRE /= aire_temp) then
						-- Si on a un nouveau p�rim�tre ou une nouvelle aire 
						-- on refait un calcul
						aire_temp <= AIRE;
						if (perim_temp = PERIM) then
							-- Si le p�rim�tre est le m�me on peut passer la multiplication
							enable_div <= '1';
							etat_act <= division;
						else
							perim_temp <= PERIM;
							etat_act <= multiplication;
						end if;
					else 
						etat_act <= attente;
					end if;
					
				when multiplication =>
					if (perim_carre /= perim_carre_temp) then
						-- Quand le p�rim�tre� est calcul� on continue
						perim_carre_temp <= perim_carre;
						enable_div <= '1';
						etat_act <= division;
					else
						etat_act <= multiplication;
					end if;
					
				when division =>
					enable_div <= '0';
					if (res_div_rdy = '1') then
						-- Quand la division est calcul� on continue
						resultat_temp <= resultat;
						etat_act <= detect_forme;
					else
						etat_act <= division;
					end if;
					
				when detect_forme =>
					if ((resultat_temp >= x"0000000B") and (resultat_temp <= x"0000000D")) then
						-- Cercle
						FORME <= "01";
					elsif ((resultat_temp >= x"0000000E") and (resultat_temp <= x"00000011")) then
						-- Carr�
						FORME <= "10";
					elsif ((resultat_temp >= x"00000013") and (resultat_temp <= x"00000015")) then
						-- Triangle
						FORME <= "11";
					else
						-- Indetermin�
						FORME <= "00";
					end if;
					
					etat_act <= fin;
					
				when fin =>
					RESULTAT_RDY <= '1';
					etat_act <= attente;
					
				when others =>
					-- TO DO 
					
			end case;
		end if;
	end process state_machine;

	outputs: process(etat_act)
	begin
		case etat_act is
			when attente =>
				-- TO DO
			when multiplication =>
				-- TO DO
			when division =>
				-- TO DO
			when detect_forme =>
				-- TO DO
			when fin =>
				-- TO DO
			when others =>
				-- TO DO
		end case;
	end process outputs;
	
end Behavioral;

