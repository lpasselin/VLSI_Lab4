----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:37:45 11/16/2017 
-- Design Name: 
-- Module Name:    RegFiltre3x3 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RegFiltre3x3 is
	generic ( 	HEIGHT : integer;
					WIDTH : integer);
    Port ( INPUT : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           ENABLE : in  STD_LOGIC;
           OUTPUT : out  STD_LOGIC_VECTOR (8 downto 0)); -- 9 bits pour filtre 3x3
end RegFiltre3x3;

architecture Behavioral of RegFiltre3x3 is
	
	signal RegOut : STD_LOGIC_VECTOR (WIDTH*2+2 downto 0) := (others => '0');
	signal RegIn: STD_LOGIC_VECTOR (WIDTH*2+2 downto 0) := (others => '0');
	
begin
	OUTPUT(0) <= RegOut(WIDTH*2+2);
	OUTPUT(1) <= RegOut(WIDTH*2+1);
	OUTPUT(2) <= RegOut(WIDTH*2);
	OUTPUT(3) <= RegOut(WIDTH+2);
	OUTPUT(4) <= RegOut(WIDTH+1);
	OUTPUT(5) <= RegOut(WIDTH);
	OUTPUT(6) <= RegOut(2);
	OUTPUT(7) <= RegOut(1);
	OUTPUT(8) <= RegOut(0);
	
	RegIn <= RegOut;
	
	MultiR : for i in 0 to (WIDTH*2+2) GENERATE
		MultiR1 : if(i = 0) generate
			Reg1bits_i: entity work.Reg1bits 
				PORT MAP(INPUT => INPUT,
							ENABLE => ENABLE,
							RESET => RESET,
							CLK => CLK,
							OUTPUT => regout(i));
		end Generate MultiR1;
		
		MultiRi : if(i > 0) generate
			Reg1bits_i: entity work.Reg1bits 
				PORT MAP(INPUT => RegIn(i-1),
							ENABLE => ENABLE,
							RESET => RESET,
							CLK => CLK,
							OUTPUT => regout(i));
		end Generate MultiRi;
	end generate MultiR;

end Behavioral;

