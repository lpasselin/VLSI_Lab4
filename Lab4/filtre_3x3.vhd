----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:30:14 11/16/2017 
-- Design Name: 
-- Module Name:    filtre_3x3 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library digilent;
use digilent.Video.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Resolution 1600x900 == WIDTHxHEIGHT
entity filtre_3x3 is
	generic (	WIDTH : natural := 1600;
					HEIGHT : natural := 900;
					THRESHOLD : integer := 4);
    Port ( INPUT : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
			  RESET: in STD_LOGIC;
			  ENABLE: in STD_LOGIC;
           VtcHCnt : in  NATURAL;
           VtcVCnt : in  NATURAL;
           OUTPUT : out  STD_LOGIC);
end filtre_3x3;

architecture Behavioral of filtre_3x3 is

	signal P : STD_LOGIC_VECTOR (8 downto 0) := (others => '0');
	signal output_enable : STD_LOGIC := '1';
	signal filter_output : STD_LOGIC := '1';
	
	signal reset_data : STD_LOGIC := '1';
	
begin
	-- TODO WITDH-1 a valider
	-- si contours, retourne 0 comme output.
	--output_enable <= ENABLE='1' and not (VtcHCnt = 0 or VtcHCnt = WIDTH-1 or VtcVCnt = 0 or VtcVCnt = HEIGHT-1);
	
	output_enable <= '1' when (ENABLE='1' and not (VtcHCnt = 0 or VtcHCnt >= WIDTH-1 or VtcVCnt = 0 or VtcVCnt >= HEIGHT-1)) else
							'0';
							
	reset_data <= '0' when (RESET='0') or (VtcHCnt = WIDTH-1 and VtcVCnt = HEIGHT-1) else
						'1';
			
			
	OUTPUT 	<= '0' when output_enable = '0' else
					filter_output when output_enable = '1' else
					'0';
	-- init reg a decalage qui retourne matrice 3x3 des valeurs
   Reg3x3: entity work.RegFiltre3x3 
	GENERIC MAP (	WIDTH=>WIDTH,
					HEIGHT=>HEIGHT)
	PORT MAP (
          RESET => reset_data,
          CLK => CLK,
          ENABLE => ENABLE,
          INPUT => INPUT,
          OUTPUT => P
        );
	
	-- init filtre 3x3
	FILTER: entity work.binary_sobel 
	GENERIC MAP (THRESHOLD=>THRESHOLD)
	PORT MAP (
			INPUT => P,
			OUTPUT => filter_output
		);

end Behavioral;

