----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:04:12 11/16/2017 
-- Design Name: 
-- Module Name:    binary_sobel - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library digilent;
use digilent.Video.ALL;
-- use IEEE.std_logic_arith.all; -- addition +

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity binary_sobel is
	generic ( THRESHOLD : integer := 4); -- TODO remove threshold variable integer. should be std_logic_vector
    Port ( INPUT : in std_logic_vector (8 downto 0);
           OUTPUT : out  STD_LOGIC);
end binary_sobel;

architecture Behavioral of binary_sobel is
	signal tmp0, tmp1, tmp2, tmp3, tmp4, tmp5, tmp6, tmp7, tmp8: STD_LOGIC_VECTOR (4 downto 0) := (others=>'0'); --optimal -1 a 1
	signal sobel_x, sobel_y, sobel_x_abs, sobel_y_abs: STD_LOGIC_VECTOR (4 downto 0) := (others=>'0'); -- optimal -4 a 4
	signal sobel_result: STD_LOGIC_VECTOR (4 downto 0) := (others=>'0'); -- optimal -8 a 8
begin
	-- TODO mieux faire
	tmp0 <= "00001" when INPUT(0)='1' else (others=>'0');
	tmp1 <= "00001" when INPUT(1)='1' else (others=>'0');
	tmp2 <= "00001" when INPUT(2)='1' else (others=>'0');
	tmp3 <= "00001" when INPUT(3)='1' else (others=>'0');
	tmp4 <= "00001" when INPUT(4)='1' else (others=>'0');
	tmp5 <= "00001" when INPUT(5)='1' else (others=>'0');
	tmp6 <= "00001" when INPUT(6)='1' else (others=>'0');
	tmp7 <= "00001" when INPUT(7)='1' else (others=>'0');
	tmp8 <= "00001" when INPUT(8)='1' else (others=>'0');
	
	-- TODO mieux faire
	sobel_x <= tmp0 + tmp3+tmp3 + tmp6 + (not(tmp2 + tmp5+tmp5 + tmp8)+"00001");
	sobel_y <= tmp0 + tmp1+tmp1 + tmp2 + (not(tmp6 + tmp7+tmp7 + tmp8)+"00001");
	
	sobel_x_abs <= "00001" + (not sobel_x) when sobel_x(4) = '1' else sobel_x;
	sobel_y_abs <= "00001" + (not sobel_y) when sobel_y(4) = '1' else sobel_y;
	
	sobel_result <= sobel_x_abs + sobel_y_abs;
	
	-- TODO use THRESHOLD value when not integer
	OUTPUT <= '1' when (sobel_result >= "00100") else '0';


end Behavioral;

