----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:10:19 10/30/2017 
-- Design Name: 
-- Module Name:    Reg1bits - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Reg1bits is
    Port ( RESET : in  STD_LOGIC;
           CLK : in  STD_LOGIC;
           ENABLE : in  STD_LOGIC;
           INPUT : in  STD_LOGIC;
           OUTPUT : out  STD_LOGIC);
end Reg1bits;

architecture Behavioral of Reg1bits is

begin
	process(CLK, RESET, ENABLE)
	begin
		if(RESET = '0') then
			OUTPUT <= '0';
			
		elsif(CLK'event and CLK = '1') then
			if(ENABLE = '1') then
				OUTPUT <= INPUT;
			end if;
		end if;
	end process;
end Behavioral;
