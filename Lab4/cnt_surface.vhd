----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:56:15 11/27/2017 
-- Design Name: 
-- Module Name:    cnt_surface - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cnt_surface is
    Port ( INPUT : in  STD_LOGIC;
			  OUTPUT_ZONE_0: out STD_LOGIC_VECTOR (31 downto 0); -- TODO optimiser (17 downto 0) ferait l'affaire
			  OUTPUT_ZONE_1: out STD_LOGIC_VECTOR (31 downto 0);
			  OUTPUT_ZONE_2: out STD_LOGIC_VECTOR (31 downto 0);
			  OUTPUT_ZONE_3: out STD_LOGIC_VECTOR (31 downto 0);
			  OUTPUT_ZONE_4: out STD_LOGIC_VECTOR (31 downto 0);
			  OUTPUT_ZONE_5: out STD_LOGIC_VECTOR (31 downto 0);
           CLK : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           ENABLE : in  STD_LOGIC;
           VtcHCnt : in  natural;
           VtcVCnt : in  natural
			 );
end cnt_surface;

architecture Behavioral of cnt_surface is
	signal perimetre_zone_0: STD_LOGIC_VECTOR (31 downto 0) := (others => '1');
	signal perimetre_zone_1: STD_LOGIC_VECTOR (31 downto 0) := (others => '1');
	signal perimetre_zone_2: STD_LOGIC_VECTOR (31 downto 0) := (others => '1');
	signal perimetre_zone_3: STD_LOGIC_VECTOR (31 downto 0) := (others => '1');
	signal perimetre_zone_4: STD_LOGIC_VECTOR (31 downto 0) := (others => '1');
	signal perimetre_zone_5: STD_LOGIC_VECTOR (31 downto 0) := (others => '1');
begin

process(CLK, ENABLE, RESET)
begin

	if (RESET='0') then
	-- TODO valider que reset pas effectue a chaque debut de parcours de matrice
		perimetre_zone_0 <= (others=> '0');
		perimetre_zone_1 <= (others=> '0');
		perimetre_zone_2 <= (others=> '0');
		perimetre_zone_3 <= (others=> '0');
		perimetre_zone_4 <= (others=> '0');
		perimetre_zone_5 <= (others=> '0');
		
		OUTPUT_ZONE_0 <= (others=> '0');
		OUTPUT_ZONE_1 <= (others=> '0');
		OUTPUT_ZONE_2 <= (others=> '0');
		OUTPUT_ZONE_3 <= (others=> '0');
		OUTPUT_ZONE_4 <= (others=> '0');
		OUTPUT_ZONE_5 <= (others=> '0');
		
		
	elsif(CLK'event and CLK = '1') then
		if(ENABLE = '1') then
			if (VtcHcnt=(1600-1) and VtcVCnt=(900-1)) then
				-- update la valeur des zones pour l'image courante 
				-- (skip le dernier pixel mais pas tr�s grave)
				-- TODO verifier si cam reset pourrait etre utilise pour pas perdre le premier pixel
				OUTPUT_ZONE_0 <= perimetre_zone_0;
				OUTPUT_ZONE_1 <= perimetre_zone_1;
				OUTPUT_ZONE_2 <= perimetre_zone_2;
				OUTPUT_ZONE_3 <= perimetre_zone_3;
				OUTPUT_ZONE_4 <= perimetre_zone_4;
				OUTPUT_ZONE_5 <= perimetre_zone_5;
				
			elsif (VtcHCnt=0 and VtcVCnt=0) then
				-- reset les valeurs de compteurs
				-- (skip le premier pixel mais pas tr�s grave)
				perimetre_zone_0 <= (others=> '0');
				perimetre_zone_1 <= (others=> '0');
				perimetre_zone_2 <= (others=> '0');
				perimetre_zone_3 <= (others=> '0');
				perimetre_zone_4 <= (others=> '0');
				perimetre_zone_5 <= (others=> '0');
			elsif (INPUT='1') then
				if (VtcVCnt < 449 and VtcHCnt < 532) then
					perimetre_zone_0 <= perimetre_zone_0 + x"0001";
				elsif (VtcVCnt < 449 and VtcHCnt < 1064) then
					perimetre_zone_1 <= perimetre_zone_1 + x"0001";
				elsif (VtcVCnt < 449) then
					perimetre_zone_2 <= perimetre_zone_2 + x"0001";
				elsif (VtcHCnt < 532) then
					perimetre_zone_3 <= perimetre_zone_3 + x"0001";
				elsif (VtcHCnt < 1064) then
					perimetre_zone_4 <= perimetre_zone_4 + x"0001";
				else
					perimetre_zone_5 <= perimetre_zone_5 + x"0001";
				end if;
			end if;
		end if;	end if;
	
	
	
end process;

end Behavioral;

