----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:47:09 11/22/2017 
-- Design Name: 
-- Module Name:    filtre_seuil - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library digilent;
use digilent.Video.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity filtre_seuil is
    Port ( DATA_INPUT: in std_logic_vector (15 downto 0);
				RGB_OUTPUT: out std_logic_vector (2 downto 0)
				);
end filtre_seuil;

architecture Behavioral of filtre_seuil is
	signal dataR, dataB: std_logic_vector (4 downto 0) := (others=>'0');
	signal dataG: std_logic_vector (5 downto 0) := (others=>'0');
	signal seuilR, seuilG, seuilB, R, G, B: std_logic := '0';
begin

--	RED_I => FbRdData(15 downto 11) & "000",
--	GREEN_I => FbRdData(10 downto 5) & "00",
--	BLUE_I => FbRdData(4 downto 0) & "000",

dataR <= DATA_INPUT(15 downto 11);
dataG <= DATA_INPUT(10 downto 5);
dataB <= DATA_INPUT(4 downto 0);

seuilR <= '1' when (dataR > "1000") else '0'; --sur 4 bits
seuilG <= '1' when (dataG > "01100") else '0'; --sur 5 bits
seuilB <= '1' when (dataB > "1000") else '0'; --sur 4 bits

-- si deux couleurs superposées, traiter comme aucune couleur
R <= seuilR and not (seuilG or seuilB);
G <= seuilG and not (seuilR or seuilB);
B <= seuilB and not (seuilR or seuilG);

RGB_OUTPUT(2) <= R;
RGB_OUTPUT(1) <= G;
RGB_OUTPUT(0) <= B;

end Behavioral;

