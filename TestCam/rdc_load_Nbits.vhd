----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:46:08 10/24/2017 
-- Design Name: 
-- Module Name:    rdc_load_Nbits - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rdc_load_Nbits is
	generic(N : integer := 8);
   Port ( RESET     : in  STD_LOGIC;
          CLK   	  : in  STD_LOGIC;
          ENABLE    : in  STD_LOGIC;
          MODE      : in  STD_LOGIC;
			 INPUT     : in  STD_LOGIC;
			 OUTPUT	  : out STD_LOGIC;
          LOAD 	  : in  STD_LOGIC_VECTOR(N-1 downto 0));
			 
end rdc_load_Nbits;

architecture Behavioral of rdc_load_Nbits is

	component OBR 
		port( clk 	 : in  STD_LOGIC;
				en 	 : in  STD_LOGIC;
				reset  : in  STD_LOGIC;
				d      : in  STD_LOGIC;
				q      : out STD_LOGIC);
	end component;
	
	component mux2to1 is
		Port ( SEL : in  STD_LOGIC;
				 A   : in  STD_LOGIC;
             B   : in  STD_LOGIC;
             Z   : out STD_LOGIC);
	end component;
	
	signal regOutput : STD_LOGIC_VECTOR(N-1 downto 0) := (others => '0');
	signal regInput : STD_LOGIC_VECTOR(N-1 downto 0) := (others => '0');
	
begin

	G0: for i in 0 to N-1 generate
		OBR0:  OBR port map(clk => clk, en => Enable, reset => reset, d => regInput(i), q => regOutput(i));
	end generate G0;
	
	G1: for i in 0 to N-1 generate
		G10: if i=0 generate
			MUX0 : mux2to1 port map(A => input, B => load(i), Z => regInput(i), Sel => MODE);
		end generate G10;
		G11: if i>0 generate
			MUX1 : mux2to1 port map(A => regOutput(i-1), B=> load(i), Z => regInput(i), Sel => MODE);
		end generate G11;
	end generate G1;
	
	OUTPUT <= regOutput(N-1);
	
end Behavioral;

