----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:43:53 09/25/2017 
-- Design Name: 
-- Module Name:    counter_8bits - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter_16bits is

	 Port ( S	   : out  STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
			  reset  : in STD_LOGIC;
			  enable : in STD_LOGIC;
			  clk    : in STD_LOGIC;
			  COUT 	: out  STD_LOGIC);
			  
end counter_16bits;

architecture Behavioral of counter_16bits is

	component halfAdder
		port(A		: in STD_LOGIC;
			  B		: in STD_LOGIC;
			  Sum		: out STD_LOGIC;
			  Carry  : out STD_LOGIC);
		end component;
	
	component fullAdder
		port(A		: in  STD_LOGIC;
			  B		: in  STD_LOGIC;
			  CIN    : in  STD_LOGIC;
			  S		: out STD_LOGIC;
			  COUT   : out STD_LOGIC);
		end component;
	
	component OBR 
		port( clk 	 : in  STD_LOGIC;
				en 	 : in  STD_LOGIC;
				reset  : in  STD_LOGIC;
				d      : in  STD_LOGIC;
				q      : out STD_LOGIC);
	end component;
	
		signal CarryOut : STD_LOGIC_VECTOR(15 downto 0);
		signal A : STD_LOGIC_VECTOR(15 downto 0) :="0000000000000001";
		signal D : STD_LOGIC_VECTOR(15 downto 0) :="0000000000000000";
		signal Q : STD_LOGIC_VECTOR(15 downto 0) :="0000000000000000" ;

begin

	HALFADDER1 : halfAdder port map(A=>A(0), B=>Q(0), Carry=>CarryOut(0), Sum => D(0));
	OBR0 : OBR port map(clk, enable, reset, D(0), Q(0));
	
	FULLADDER1 : fullAdder port map(A=>A(1), B=>Q(1), CIN=>CarryOut(0), COUT => CarryOut(1), S=>D(1));
	OBR1 : OBR port map(clk, enable, reset, D(1), Q(1));
	
	FULLADDER2 : fullAdder port map(A=>A(2), B=>Q(2), CIN=>CarryOut(1), COUT => CarryOut(2), S=>D(2));
	OBR2 : OBR port map(clk, enable, reset, D(2), Q(2));
	
	FULLADDER3 : fullAdder port map(A=>A(3), B=>Q(3), CIN=>CarryOut(2), COUT => CarryOut(3), S=>D(3));
	OBR3 : OBR port map(clk, enable, reset, D(3), Q(3));
	
	FULLADDER4 : fullAdder port map(A=>A(4), B=>Q(4), CIN=>CarryOut(3), COUT => CarryOut(4), S=>D(4));
	OBR4 : OBR port map(clk, enable, reset, D(4), Q(4));
	
	FULLADDER5 : fullAdder port map(A=>A(5), B=>Q(5), CIN=>CarryOut(4), COUT => CarryOut(5), S=>D(5));
	OBR5 : OBR port map(clk, enable, reset, D(5), Q(5));
	
	FULLADDER6 : fullAdder port map(A=>A(6), B=>Q(6), CIN=>CarryOut(5), COUT => CarryOut(6), S=>D(6));
	OBR6 : OBR port map(clk, enable, reset, D(6), Q(6));
	
	FULLADDER7 : fullAdder port map(A=>A(7), B=>Q(7), CIN=>CarryOut(6), COUT => CarryOut(7), S=>D(7));
	OBR7 : OBR port map(clk, enable, reset, D(7), Q(7));
	
	FULLADDER8 : fullAdder port map(A=>A(8), B=>Q(8), CIN=>CarryOut(7), COUT => CarryOut(8), S=>D(8));
	OBR8 : OBR port map(clk, enable, reset, D(8), Q(8));
	
	FULLADDER9 : fullAdder port map(A=>A(9), B=>Q(9), CIN=>CarryOut(8), COUT => CarryOut(9), S=>D(9));
	OBR9 : OBR port map(clk, enable, reset, D(9), Q(9));
	
	FULLADDER10 : fullAdder port map(A=>A(10), B=>Q(10), CIN=>CarryOut(9), COUT => CarryOut(10), S=>D(10));
	OBR10 : OBR port map(clk, enable, reset, D(10), Q(10));
	
	FULLADDER11 : fullAdder port map(A=>A(11), B=>Q(11), CIN=>CarryOut(10), COUT => CarryOut(11), S=>D(11));
	OBR11 : OBR port map(clk, enable, reset, D(11), Q(11));
	
	FULLADDER12 : fullAdder port map(A=>A(12), B=>Q(12), CIN=>CarryOut(11), COUT => CarryOut(12), S=>D(12));
	OBR12 : OBR port map(clk, enable, reset, D(12), Q(12));
	
	FULLADDER13 : fullAdder port map(A=>A(13), B=>Q(13), CIN=>CarryOut(12), COUT => CarryOut(13), S=>D(13));
	OBR13 : OBR port map(clk, enable, reset, D(13), Q(13));
	
	FULLADDER14 : fullAdder port map(A=>A(14), B=>Q(14), CIN=>CarryOut(13), COUT => CarryOut(14), S=>D(14));
	OBR14 : OBR port map(clk, enable, reset, D(14), Q(14));
	
	FULLADDER15 : fullAdder port map(A=>A(15), B=>Q(15), CIN=>CarryOut(14), COUT => CarryOut(15), S=>D(15));
	OBR15 : OBR port map(clk, enable, reset, D(15), Q(15));

	S <= Q;
	COUT <= CarryOut(15);
	
end Behavioral;

