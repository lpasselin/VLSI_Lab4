----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:53:14 11/23/2017 
-- Design Name: 
-- Module Name:    PhotoStateMachine - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PhotoStateMachine is
    Port ( clk							: in 	 std_logic;
			  verticalCounter 		: in   natural;
			  horizontalCounter 	   : in 	 natural;
			  SW_I						: in   STD_LOGIC_VECTOR(7 downto 0);
			  camADV						: in 	 STD_LOGIC;
			  LED_O						: out   STD_LOGIC_VECTOR(7 downto 0);
           CamEnable 				: out  STD_LOGIC);
			  
end PhotoStateMachine;

architecture Behavioral of PhotoStateMachine is

	type state_type is (Waiting, HavingPicture, Stop);
	signal state : state_type;	
	
begin
		LED_O(3) <= '0';
		LED_O(4) <= '0';
		LED_O(5) <= '0';
		LED_O(6) <= '0';
		LED_O(7) <= '0';

process(Clk)
	begin		
		if(Clk ='1' and Clk'event) then
			case state is
				when Waiting =>
					LED_O(0) <= '1';
					LED_O(1) <= '0';
					LED_O(2) <= '0';
					CamEnable <= camADV;
					if(verticalCounter = 0 and horizontalCounter = 0 and SW_I(0) = '1') then
						state <= HavingPicture;
					end if;
				when HavingPicture =>
					LED_O(0) <= '0';
					LED_O(1) <= '1';
					LED_O(2) <= '0';
					CamEnable <= camADV;
					if(verticalCounter = 900 and horizontalCounter = 1600) then
						state <= Stop;
					end if;
				when Stop =>
					LED_O(0) <= '0';
					LED_O(1) <= '0';
					LED_O(2) <= '1';
					CamEnable <= '0';
					if(verticalCounter = 0 and horizontalCounter = 0 and SW_I(0) = '0') then
						state <= Waiting;
					end if;
				end case;
		end if;
	end process;


end Behavioral;

