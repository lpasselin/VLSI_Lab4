----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:26:02 11/04/2017 
-- Design Name: 
-- Module Name:    transmetteur_UART - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity transmetteur_UART is
    Port ( Start    : in  STD_LOGIC;
           Clk      : in  STD_LOGIC;
           Reset    : in  STD_LOGIC;
           Data_In  : in  STD_LOGIC_VECTOR (7 downto 0);
           Occupe   : out  STD_LOGIC;
           Termine  : out  STD_LOGIC;
           Tx : out  STD_LOGIC);
			  
end transmetteur_UART;
	
architecture Behavioral of transmetteur_UART is

	component rdc_load_Nbits is
		generic(N : integer := 8);
		Port ( RESET     : in  STD_LOGIC;
				 CLK   	  : in  STD_LOGIC;
				 ENABLE    : in  STD_LOGIC;
				 MODE      : in  STD_LOGIC;
				 INPUT     : in  STD_LOGIC;
				 OUTPUT	  : out STD_LOGIC;
				 LOAD 	  : in  STD_LOGIC_VECTOR(N-1 downto 0));
	end component;

	component counter_16bits is

		 Port ( S	   : out  STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
				  Reset  : in STD_LOGIC;
				  enable : in STD_LOGIC;
				  Clk    : in STD_LOGIC;
				  Cout 	: out  STD_LOGIC);
				  
	end component;

	component comparator_16bits is
		 Port ( A : in  STD_LOGIC_VECTOR (15 downto 0);
				  B : in  STD_LOGIC_VECTOR (15 downto 0);
				  CMP : out  STD_LOGIC);
	end component;

	type state_type is (Idle, Load,StartBit, DataBit, StopBit, Done);
	signal state : state_type;	
	
	--RDC Signals
	signal RegEnable : STD_LOGIC;
	signal RegMode   : STD_LOGIC;
	signal RegOutput : STD_LOGIC;
	--Baud Signals
	signal BaudRate   		: STD_LOGIC_VECTOR(15 downto 0) :=x"28B0";
	signal BaudCounterCount	: STD_LOGIC_VECTOR(15 downto 0) :=x"0000";
	signal BaudChangeState  : STD_LOGIC;
	signal BaudReset			: STD_LOGIC;
	signal BaudEnable			: STD_LOGIC;
	--Data Signals
	signal DataLength   		: STD_LOGIC_VECTOR(15 downto 0) :=x"0008";
	signal DataCounterCount	: STD_LOGIC_VECTOR(15 downto 0) :=x"0000";
	signal DataSent         : STD_LOGIC;
	signal DataReset			: STD_LOGIC;
	signal DataEnable			: STD_LOGIC;
	
begin

	RDC 				 : rdc_load_Nbits    port map (Reset => Reset, Clk => Clk, Enable => RegEnable, Mode => RegMode, Input => '1', Output => RegOutput, Load => Data_In);
	
	BaudCounter     : counter_16bits    port map (S=>BaudCounterCount, Reset=>BaudReset, enable=>BaudEnable, Clk=>Clk);
	BaudComparator  : comparator_16bits port map (A=>BaudCounterCount, B=>BaudRate, Cmp=>BaudChangeState);
	
	DataCounter     : counter_16bits    port map (S=>DataCounterCount, Reset=>DataReset, enable=>DataEnable, Clk=>Clk);
	DataComparator  : comparator_16bits port map (A=>DataCounterCount, B=>DataLength, Cmp=>DataSent);
	
	process(Reset, Clk)
	begin
		if(Reset = '0') then
		
				state		  <= Idle;
				Tx			  <= '1';
				RegEnable  <= '0';
				RegMode    <= '0';
				BaudReset  <= '0';
				BaudEnable <= '0';
				DataReset  <= '0';
				DataEnable <= '0';
				Termine    <= '0';
				Occupe     <= '0';
				
		elsif(Clk ='1' and Clk'event) then
			case state is
			
				when Idle =>
					Tx			  <= '1';
					RegEnable  <= '0';
					RegMode    <= '1';
					BaudReset  <= '0';
					BaudEnable <= '0';
					DataReset  <= '0';
					DataEnable <= '0';
					Termine    <= '0';
					Occupe     <= '0';
					
					if(Start = '1') then
						state <= Load;
					end if;
					
				when Load =>
				
					Tx          <= '1';
					RegMode		<= '1';
					RegEnable   <= '1';
					BaudReset   <= '1';
					BaudEnable  <= '0';
					DataReset   <= '0';
					DataEnable  <= '0';
					Termine     <= '0';
					Occupe      <= '1';
					
					state <= StartBit;
					
				when StartBit =>
				
					Tx          <= '0';
					BaudReset   <= '1';
					BaudEnable  <= '1';
					RegMode		<= '1';
					RegEnable   <= '1';
					DataReset   <= '0';
					DataEnable  <= '0';
					Termine     <= '0';
					Occupe      <= '1';
					
					if(BaudChangeState = '1') then
						state     <= DataBit;
						BaudReset <= '0';
					end if;
					
				when DataBit =>
				
					Tx 			 <= RegOutput;
					
					RegMode      <= '0';
					DataReset    <= '1';
					BaudEnable	 <= '1';
					Termine      <= '0';
					Occupe       <= '1';
					
					if(BaudChangeState = '1') then
						BaudReset    <= '0';
						RegEnable    <= '1';
						DataEnable   <= '1';
					else 
						BaudReset    <= '1';
						RegEnable 	 <= '0';
						DataEnable   <= '0';
					end if;
					
					if(DataSent = '1') then
						state <= StopBit;
						BaudReset <= '0';
					end if;
					
				when StopBit =>
					Tx 			 <= '1';
					DataEnable   <= '0';
					DataReset    <= '0';
					RegEnable    <= '0';
					RegMode      <= '0';
					BaudReset    <= '1';
					BaudEnable   <= '1';
					Termine      <= '0';
					Occupe       <= '1';
					
					if(BaudChangeState = '1') then
						state <= Done;
					end if;
					
				when Done =>
							
					Tx 			 <= '1';
					DataEnable   <= '0';
					DataReset    <= '0';
					RegEnable    <= '0';
					RegMode      <= '0';
					BaudReset    <= '1';
					BaudEnable   <= '1';
					Termine      <= '1';
					Occupe       <= '0';
					
				end case;
		end if;
	end process;

end Behavioral;

