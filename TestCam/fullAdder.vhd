----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:07:13 10/22/2015 
-- Design Name: 
-- Module Name:    full_adder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fullAdder is
    Port ( A 		: in  STD_LOGIC;
           B 		: in  STD_LOGIC;
           CIN 	: in  STD_LOGIC;
           S 		: out  STD_LOGIC;
           COUT 	: out  STD_LOGIC);
end fullAdder;

architecture Behavioral of fullAdder is

	component halfAdder
		port(A   : in STD_LOGIC;
			  B   : in STD_LOGIC;
			  Sum : out STD_LOGIC;
			  Carry : out STD_LOGIC);
	end component;
	
	signal HalfAdder1Carry : STD_LOGIC;
	signal HalfAdder2Carry : STD_LOGIC;
	signal HalfAdder1Sum   : STD_LOGIC;
	
begin
		
		HALFADDER1: halfAdder port map(A=>A, B=>B, Sum =>HalfAdder1Sum, Carry => HalfAdder1Carry);
		HALFADDER2: halfAdder port map(A=>HalfAdder1Sum, B=>CIN, Sum=> S, Carry => HalfAdder2Carry);
		
		COUT <= HalfAdder1Carry or HalfAdder2Carry;

end Behavioral;

