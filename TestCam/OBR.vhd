----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:44:39 10/24/2017 
-- Design Name: 
-- Module Name:    OBR - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity OBR is
    Port ( reset : in  STD_LOGIC;
           clk   : in  STD_LOGIC;
           en    : in  STD_LOGIC;
           d     : in  STD_LOGIC;
           q     : out  STD_LOGIC);
end OBR;

architecture Behavioral of OBR is

begin
	Process(reset, clk)
	begin
		if(reset='0') then
			q<='0';
		elsif(clk'event and clk='1') then
			if(en = '1') then
				q<=d;
			end if;
		end if;
	end process;
end Behavioral;

